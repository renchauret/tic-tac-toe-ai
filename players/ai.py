import json

from complex_encoder import ComplexEncoder
from players.neuralNet.brain import Brain, random_brain_definition

debug = False


class Ai:
    brain = None
    wins = None

    def __init__(self, brain_definition=None):
        if brain_definition is None:
            brain_definition = random_brain_definition()
        self.brain = Brain(brain_definition=brain_definition)
        self.wins = 0
        if debug:
            print(json.dumps(self.brain.definition.reprJSON(), cls=ComplexEncoder))

    def make_move(self, board, valid_moves):
        best_move = valid_moves[0]
        best_score = -1
        for move in valid_moves:
            input_values = [move[0], move[1]]
            for row in board:
                for cell in row:
                    input_values.append(cell)
            output_value = self.brain.activate(input_values)[0]
            if debug:
                print("Move: " + str(move) + " Score: " + str(output_value))
            if output_value > best_score:
                best_score = output_value
                best_move = move
        return best_move

    def reproduce(self):
        new_brain_def = self.brain.definition.reproduce()
        return Ai(brain_definition=new_brain_def)
