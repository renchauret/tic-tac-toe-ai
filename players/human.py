class Human:
    def make_move(self, board, valid_moves):
        print("Valid moves:")
        print(valid_moves)
        move = [-1, -1]
        while move not in valid_moves:
            print("Enter a valid row and column")
            row = input("Row: ")
            try:
                row = int(row)
            except:
                print(row + " is not a valid integer")
                continue
            col = input("Col: ")
            try:
                col = int(col)
            except:
                print(col + " is not a valid integer")
                continue
            move = [row, col]
        return move
