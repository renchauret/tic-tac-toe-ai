import math
from enum import Enum


class NodeType(Enum):
    INPUT = 1
    NODE = 2
    OUTPUT = 3


class Connection:
    input_node = None
    output_node = None
    weight = None

    def __init__(self, input_node, output_node, weight=1):
        self.input_node = input_node
        self.output_node = output_node
        self.weight = weight


class Node:
    node_type = None
    value = None
    connections = None

    def __init__(self, node_type=NodeType.INPUT, connections=None):
        if connections is None:
            connections = []
        self.node_type = node_type
        self.connections = connections
        self.value = 0

    def add_connection(self, connection):
        self.connections.append(connection)

    def add_value(self, value):
        self.value += value

    def activate(self):
        calc_value = math.tanh(self.value)
        for connection in self.connections:
            connection.output_node.add_value(calc_value * connection.weight)
        self.value = 0
        return calc_value
