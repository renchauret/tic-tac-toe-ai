import copy
import json
import random
from enum import Enum
from types import SimpleNamespace

from players.neuralNet.node import Node, NodeType, Connection


class ConnectionType(str, Enum):
    IO = "IO"
    IN = "IN"
    NN = "NN"
    NO = "NO"


class ConnectionDefinition:
    connection_type = None
    input_index = None
    output_index = None
    weight = None

    def __init__(self, connection_type=ConnectionType.IO, input_index=0, output_index=0, weight=1.0):
        self.connection_type = connection_type
        self.input_index = input_index
        self.output_index = output_index
        self.weight = weight

    def reprJSON(self):
        return dict(connection_type=self.connection_type, input_index=self.input_index, output_index=self.output_index,
                    weight=self.weight)


class BrainDefinition:
    num_inputs = None
    num_nodes = None
    num_outputs = None
    connection_defs = None

    def __init__(self, num_inputs=11, num_nodes=3, num_outputs=1, connection_defs=None):
        if connection_defs is None:
            connection_defs = [ConnectionDefinition(ConnectionType.IO, 0, 0, 1)]
        self.num_inputs = num_inputs
        self.num_nodes = num_nodes
        self.num_outputs = num_outputs
        self.connection_defs = connection_defs

    def reproduce(self, mutation_chance=0.5):
        new_brain_def = copy.deepcopy(self)
        mutation_roll = random.uniform(0, 1)
        if mutation_roll <= mutation_chance:
            new_brain_def.mutate()
        return new_brain_def

    # add a node, remove a node, add a connection, remove a connection, change a weight
    def mutate(self):
        # TODO:  Add back node mutations
        mutation_type = random.randint(3, 5)
        if mutation_type == 1:
            self.num_nodes += 1
        elif mutation_type == 2:
            if self.num_nodes == 0:
                return
            node_to_remove = 0
            if self.num_nodes > 1:
                node_to_remove = random.randint(0, self.num_nodes - 1)
            connections_to_remove = []
            self.num_nodes -= 1
            for connection_def in self.connection_defs:
                if connection_def.connection_type == ConnectionType.IN or connection_def.connection_type == ConnectionType.NN:
                    if connection_def.output_index == node_to_remove:
                        connections_to_remove.append(connection_def)
                    elif connection_def.output_index > node_to_remove:
                        connection_def.output_index -= 1
                if connection_def.connection_type == ConnectionType.NO or connection_def.connection_type == ConnectionType.NN:
                    if connection_def.input_index == node_to_remove and connection_def not in connections_to_remove:
                        connections_to_remove.append(connection_def)
                    elif connection_def.input_index > node_to_remove:
                        connection_def.input_index -= 1
            for connection_def in connections_to_remove:
                self.connection_defs.remove(connection_def)
        elif mutation_type == 3:
            self.connection_defs.append(random_connection_def(self.num_inputs, self.num_nodes, self.num_outputs))
        elif mutation_type == 4:
            if len(self.connection_defs) == 0:
                return
            index_to_remove = 0
            if len(self.connection_defs) > 1:
                index_to_remove = random.randint(0, len(self.connection_defs) - 1)
            del self.connection_defs[index_to_remove]
        elif mutation_type == 5:
            if len(self.connection_defs) == 0:
                return
            index_to_change = 0
            if len(self.connection_defs) > 0:
                index_to_change = random.randint(0, len(self.connection_defs) - 1)
            new_weight = random_weight()
            self.connection_defs[index_to_change].weight = new_weight

    def reprJSON(self):
        return dict(num_inputs=self.num_inputs, num_nodes=self.num_nodes, num_outputs=self.num_outputs,
                    connection_defs=self.connection_defs)


def random_weight():
    return random.uniform(-4, 4)


def random_connection_def(num_inputs=11, num_nodes=0, num_outputs=1):
    connection_type = 1
    if num_nodes > 0:
        connection_type = random.randint(1, 4)

    if connection_type == 1:
        connection_type = ConnectionType.IO
    elif connection_type == 2:
        connection_type = ConnectionType.IN
    elif connection_type == 3:
        connection_type = ConnectionType.NN
    elif connection_type == 4:
        connection_type = ConnectionType.NO

    weight = random_weight()
    input_index = 0
    output_index = 0
    if connection_type == ConnectionType.IO:
        input_index = random.randint(0, num_inputs - 1)
        output_index = 0
        if num_outputs > 1:
            output_index = random.randint(0, num_outputs - 1)
    elif connection_type == ConnectionType.IN:
        input_index = random.randint(0, num_inputs - 1)
        output_index = 0
        if num_nodes > 1:
            output_index = random.randint(0, num_nodes - 1)
    elif connection_type == ConnectionType.NN:
        input_index = 0
        if num_nodes > 1:
            input_index = random.randint(0, num_nodes - 1)
        output_index = 0
        if num_nodes > 1:
            output_index = random.randint(0, num_nodes - 1)
    elif connection_type == ConnectionType.NO:
        input_index = 0
        if num_nodes > 1:
            input_index = random.randint(0, num_nodes - 1)
        output_index = 0
        if num_outputs > 1:
            output_index = random.randint(0, num_outputs - 1)
    return ConnectionDefinition(connection_type=connection_type, input_index=input_index, output_index=output_index,
                                weight=weight)


def random_brain_definition(num_inputs=11, num_outputs=1):
    num_nodes = random.randint(5, 10)
    num_connections = random.randint(20, 50)
    connection_defs = []
    for i in range(0, num_connections):
        connection_defs.append(random_connection_def(num_inputs, num_nodes, num_outputs))
    return BrainDefinition(num_nodes=num_nodes, connection_defs=connection_defs)


def brain_definition_from_json(data):
    # Parse JSON into an object with attributes corresponding to dict keys.
    x = json.loads(data, object_hook=lambda d: SimpleNamespace(**d))
    connection_defs = []
    for connection_def in x.connection_defs:
        connection_defs.append(ConnectionDefinition(connection_type=connection_def.connection_type,
                                                    input_index=connection_def.input_index,
                                                    output_index=connection_def.output_index,
                                                    weight=connection_def.weight))
    return BrainDefinition(num_nodes=x.num_nodes, num_inputs=x.num_inputs, num_outputs=x.num_outputs,
                           connection_defs=connection_defs)


class Brain:
    inputs = None
    nodes = None
    outputs = None
    connections = None
    definition = None

    def __init__(self, brain_definition=BrainDefinition()):
        self.inputs = []
        self.nodes = []
        self.outputs = []
        self.connections = []
        self.definition = brain_definition
        self.wire(brain_definition.num_inputs, brain_definition.num_nodes, brain_definition.num_outputs,
                  brain_definition.connection_defs)

    def wire(self, num_inputs, num_nodes, num_outputs, connection_defs):
        for i in range(0, num_inputs):
            self.inputs.append(Node())

        for i in range(0, num_nodes):
            self.nodes.append(Node(node_type=NodeType.NODE))

        for i in range(0, num_outputs):
            self.outputs.append(Node(node_type=NodeType.OUTPUT))

        for connection_def in connection_defs:
            input_index = connection_def.input_index
            output_index = connection_def.output_index
            input_node = None
            output_node = None
            if connection_def.connection_type == ConnectionType.IO:
                input_node = self.inputs[input_index]
                output_node = self.outputs[output_index]
            elif connection_def.connection_type == ConnectionType.IN:
                input_node = self.inputs[input_index]
                output_node = self.nodes[output_index]
            elif connection_def.connection_type == ConnectionType.NN:
                input_node = self.nodes[input_index]
                output_node = self.nodes[output_index]
            elif connection_def.connection_type == ConnectionType.NO:
                input_node = self.nodes[input_index]
                output_node = self.outputs[output_index]

            connection = Connection(input_node=input_node, output_node=output_node, weight=connection_def.weight)
            self.connections.append(connection)
            input_node.add_connection(connection)

    def activate(self, input_values):
        for i, input_node in enumerate(self.inputs):
            input_node.add_value(input_values[i])
            input_node.activate()

        for node in self.nodes:
            node.activate()

        outputs = []
        for output in self.outputs:
            outputs.append(output.activate())

        return outputs

    def to_string(self):
        return "Number of inputs: {num_inputs}\nNumber of outputs: {num_outputs}\nNodes:".format(
            num_inputs=len(self.inputs),
            num_outputs=len(self.outputs))
