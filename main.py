import json
import random

from complex_encoder import ComplexEncoder
from game.game import play_game, GameType
from players.ai import Ai
from players.human import Human
from players.neuralNet.brain import brain_definition_from_json


def get_players(local_game_type):
    local_players = {
        1: Ai(),
        2: Ai()
    }
    if local_game_type == GameType.PVP:
        local_players = {
            1: Human(),
            2: Human()
        }
    elif local_game_type == GameType.PVA:
        coin_flip = random.randint(0, 1)
        if coin_flip == 0:
            local_players = {
                1: Human(),
                2: Ai()
            }
        else:
            local_players = {
                1: Ai(),
                2: Human()
            }

    return local_players


def pop_random(lst):
    idx = random.randrange(0, len(lst))
    return lst.pop(idx)


def get_pairs(lst):
    pairs = []
    while lst and len(lst) > 1:
        rand1 = pop_random(lst)
        rand2 = pop_random(lst)
        pair = rand1, rand2
        pairs.append(pair)
    return pairs


# if __name__ == '__main__':
#     max_gens = 1000
#     players = []
#     for i in range(0, 50):
#         players.append(Ai())
#
#     # generations
#     i = 0
#     while len(players) > 1:
#         i += 1
#         if i % 100 == 0:
#             print("Generation " + str(i))
#         pairs = get_pairs(players)
#
#         should_print = False
#         if len(pairs) == 1:
#             should_print = True
#             print("It's the championship! Printing all games.")
#
#         # series of games
#         for pair in pairs:
#             game_players = {1: pair[0], 2: pair[1]}
#             wins = {1: 0, 2: 0}
#
#             # games
#             for i2 in range(0, 7):
#                 if game_players[1].wins >= 20 and i2 == 0:
#                     print("Player 1 has a " + str(game_players[1].wins) + " win streak. Printing 1st game.")
#                     should_print = True
#                 if game_players[2].wins >= 20 and i2 == 0:
#                     print("Player 2 has a " + str(game_players[2].wins) + " win streak. Printing 1st game.")
#                     should_print = True
#                 game_winner = play_game(game_players, should_print)
#                 if game_winner != 0:
#                     wins[game_winner] += 1
#             # print("Wins: " + str(wins))
#             overall_winner = 0
#             if wins[2] > wins[1]:
#                 overall_winner = 2
#             elif wins[1] > wins[2]:
#                 overall_winner = 1
#             else:
#                 draw_check = 0
#                 while overall_winner == 0:
#                     # if draw_check == 10:
#                     #     print("Many draws.  Printing game.")
#                     # overall_winner = play_game(game_players, draw_check == 10)
#                     overall_winner = play_game(game_players, False)
#                     draw_check += 1
#                     if draw_check > 20:
#                         overall_winner = random.randint(1, 2)
#                         # print("Breaking due to many draws.  Random winner: " + str(overall_winner))
#
#             winner = game_players[overall_winner]
#             winner.wins += 1
#             if winner.wins >= 20:
#                 print(str(winner.wins) + " win streak:")
#                 print(json.dumps(winner.brain.definition.reprJSON(), cls=ComplexEncoder))
#             players.append(winner)
#             if i <= max_gens:
#                 players.append(winner.reproduce())
#     print("Final player:")
#     for player in players:
#         print(json.dumps(player.brain.definition.reprJSON(), cls=ComplexEncoder))


if __name__ == '__main__':
    data = input("Input brain definition JSON:\n")
    brain_definition = brain_definition_from_json(data)
    players = {
        1: Human(),
        2: Ai(brain_definition=brain_definition)
    }
    winner = play_game(players, True)


# if __name__ == '__main__':
#     print("Let's play Tic Tac Toe!")
#     print("Enter your game type:")
#     print("1: PvP")
#     print("2: PvAI")
#     print("3: AIvAI")
#     game_type = -1
#     while not isinstance(game_type, GameType):
#         game_type = input("Enter a number: ")
#         try:
#             game_type = GameType(int(game_type))
#         except:
#             print("Not a valid type number")
#     players = get_players(game_type)
#
#     num_games = 0
#     while not num_games >= 1:
#         num_games = input("How many games should be played per generation? ")
#         try:
#             num_games = int(num_games)
#         except:
#             print("Not a valid number")
#     num_generations = 0
#     while not num_generations >= 1:
#         num_generations = input("How many generations should be played? ")
#         try:
#             num_generations = int(num_generations)
#         except:
#             print("Not a valid number")
#     should_print = (num_games <= 10 and num_generations <= 3) or game_type != GameType.AVA
#
#     wins = {1: 0, 2: 0}
#
#     for i in range(0, num_games):
#         winner = play_game(players, should_print)
#         wins[winner] += 1
#     print("Wins: " + str(wins))
#     overall_winner = 1
#     if wins[2] > wins[1]:
#         overall_winner = 2
#     if game_type == GameType.AVA:
#         print(json.dumps(players[overall_winner].brain.definition.reprJSON(), cls=ComplexEncoder))
