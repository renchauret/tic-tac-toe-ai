# 0 = empty, 1 = player 1, 2 = player 2
def fresh_board():
    return [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]
    ]


def set_board_value(board, player, coordinates):
    board[coordinates[0]][coordinates[1]] = player


def get_valid_moves(board):
    moves = []
    for index1, row in enumerate(board):
        for index2, cell in enumerate(row):
            if cell == 0:
                moves.append([index1, index2])
    return moves


def get_board_for_player(board, player=1):
    board_for_player = []
    for row in board:
        row_for_player = []
        for cell in row:
            if cell == player:
                row_for_player.append(1)
            elif cell == 0:
                row_for_player.append(0)
            else:
                row_for_player.append(-1)
        board_for_player.append(row_for_player)
    return board_for_player


def get_winner(board):
    for row in board:
        row_won = True
        first_value = row[0]
        if first_value == 0:
            continue
        for cell in row:
            if cell != first_value:
                row_won = False
                break
        if row_won:
            return first_value

    for col_id, first_value in enumerate(board[0]):
        if first_value == 0:
            continue
        col_won = True
        for row_id in range(0, len(board)):
            cell = board[row_id][col_id]
            if cell != first_value:
                col_won = False
                break
        if col_won:
            return first_value

    diag_1_value = board[0][0]
    diag_1_won = True
    diag_2_value = board[0][len(board[0]) - 1]
    diag_2_won = True
    for row_id, row in enumerate(board):
        cell = board[row_id][row_id]
        if cell != diag_1_value:
            diag_1_won = False
        cell_2 = board[row_id][len(board[0]) - 1 - row_id]
        if cell_2 != diag_2_value:
            diag_2_won = False
        if not (diag_1_won or diag_2_won):
            break
    if diag_1_won and diag_1_value != 0:
        return diag_1_value
    if diag_2_won and diag_2_value != 0:
        return diag_2_value

    return 0


def print_board(board):
    top = "  |"
    for i in range(0, len(board)):
        top += " " + str(i)
    print(top)

    divider = "———"
    for i in range(0, len(board)):
        divider += "——"
    print(divider)

    for row_id, row in enumerate(board):
        row_string = str(row_id) + " |"
        for cell in row:
            row_string += " " + str(cell)
        print(row_string)
