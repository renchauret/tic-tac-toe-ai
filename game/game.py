import random
from enum import Enum

from game.board import get_winner, fresh_board, set_board_value, get_valid_moves, print_board, get_board_for_player


class GameType(Enum):
    PVP = 1
    PVA = 2
    AVA = 3


def play_game(players, should_print):
    board = fresh_board()

    if should_print:
        print("All coordinates are [row, column]. Index counting begins with 0. Rows count top to bottom. Columns "
              "count left to right.")

    turn = random.randint(1, 2)
    winner = get_winner(board)
    valid_moves = get_valid_moves(board)
    while winner == 0 and len(valid_moves) > 0:
        if should_print:
            print_board(board)
            print("Player " + str(turn) + "'s turn")
        board_for_player = get_board_for_player(board, turn)
        move = players[turn].make_move(board_for_player, valid_moves)
        set_board_value(board, turn, move)

        if turn == 1:
            turn = 2
        else:
            turn = 1
        winner = get_winner(board)
        valid_moves = get_valid_moves(board)

    if should_print:
        print_board(board)
        print("Game over. Winner: " + str(winner))

    return winner
